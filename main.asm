%include "dict.inc"
%include "lib.inc"
%include "words.inc"

section .rodata
keyNotFound:    db "There is no such key", 0x0
%define SIZE 256

section .bss
bufferMain: resb 255

section .text

global _start
_start:
        mov     rdi, bufferMain
        mov     rsi, SIZE
        call    read_word
        mov     rdi, bufferMain
        mov     rsi, paradise
        call    find_word
        test    rax, rax
        je      .notFoundMain
        mov     rdi, rax
        add     rdi, 8
        call    string_length
        add     rdi, rax
        inc     rdi
        call    print_string
        call    print_newline
        jmp     exit
        .notFoundMain:
                mov     rdi, keyNotFound
                call    print_str_err
                jmp     exit

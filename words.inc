%include "colon.inc"

section .rodata

colon phone, `Phone`
db "Phone word", 0

colon paper, 'Paper'
db "Paper word", 0

colon popcorn, `Popcorn`
db "Popcorn word", 0

colon paradise, `Paradise two`
db "Paradise word", 0

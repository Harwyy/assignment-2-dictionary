import subprocess
list_input = ["", "Popcorn", "Paradise two", "Hell", "Утренний туман уходит тихноько туда, куда ему надо... | Осенний денек. Макушки больших кипарисов склонились на бок... | На улице снег сечет плащи из соломы - путники идут... | Если вы перестали делать какие-то вещи просто для удовольствия, считайте, что вы больше не живете."]
list_output = ["", "Popcorn word", "Paradise word", "", ""]
list_error = ["There is no such key", "", "", "There is no such key", "There is no such key"]
for i in range (len(list_input)):
    # создание подпроцесса
    sp = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = sp.communicate(input=list_input[i].encode())
    out = stdout.decode().strip()
    err = stderr.decode().strip()
    ret_c = sp.returncode
    if (out == list_output[i] and err == list_error[i]):
        print("Test ", i + 1,": Success")
        if (out == ""):
            print("Это вывелось в error: ", err)
        else:
            print("Это вывелось в out: ", out)
    else:
        print(out)
        print(err)
        print("Test: ", i + 1, ": Fail")

%include "lib.inc"

section .text

global find_word

find_word:
        push    r11
        push    r12
        mov     r11, rdi
        mov     r12, rsi
        .iter:
                mov rdi, r11
                mov rsi, r12
                add rsi, 8
                call string_equals
                test rax, rax
                jne .endFindWord
                cmp     qword[r12], 0
                mov     r12, qword[r12]
                jne     .iter
        .endFindWord:
                mov rax, r12
                pop r12
                pop r11
                ret
